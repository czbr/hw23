FROM python:3.9.6 as build
ENV VAR=80
WORKDIR ./hw9
COPY hw9.py .
RUN python3 hw9.py -o result .

FROM nginx:1.21.1
WORKDIR hw9/result
COPY --from=build ./hw9 ./hw9/result
